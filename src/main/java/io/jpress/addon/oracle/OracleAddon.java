package io.jpress.addon.oracle;


import java.util.List;

import io.jboot.utils.StrUtil;
import io.jpress.JPressOptions;
import io.jpress.core.addon.Addon;
import io.jpress.core.addon.AddonInfo;
import io.jpress.core.menu.MenuGroup;
import io.jpress.core.menu.MenuManager;

public class OracleAddon implements Addon {

	public static final String MENU_KEY = "oracle";
	
	public static String DB_KEY="";

	public void onInstall(AddonInfo addonInfo) {
		DB_KEY=addonInfo.getId();
		// TODO Auto-generated method stub
		// 初始化DB参数
		String dbconfig = JPressOptions.get(addonInfo.getId() + ".db.config");
		if (StrUtil.isBlank(dbconfig)) {
			JPressOptions.set(addonInfo.getId() + ".db.config", AddonRecordPluginUtil.getFileText(addonInfo,"def_db.txt"));
		}
		AddonRecordPluginUtil.initDB(addonInfo);
	}

	public void onUninstall(AddonInfo addonInfo) {
		// TODO Auto-generated method stub

	}

	public void onStart(AddonInfo addonInfo) {
		DB_KEY=addonInfo.getId();
		// TODO Auto-generated method stub
        boolean found = false;
        List<MenuGroup> groups=MenuManager.me().getSystemMenus();
        for(int i=0;i<groups.size();i++) {
        	if( groups.get(i).getId() == MENU_KEY) {
        		found = true;
        		break;
        	}
        }
        if(!found) {
    		MenuGroup mapMenuGroup = new MenuGroup();
        	mapMenuGroup.setId(MENU_KEY);
        	mapMenuGroup.setText("Oracle数据库");
        	mapMenuGroup.setIcon("<i class=\"fa fa-fw fa-map\"></i>");
        	mapMenuGroup.setOrder(1);
        	groups.add(mapMenuGroup);
        }
		// 启动数据库插件
		AddonRecordPluginUtil.start(addonInfo);
	}

	public void onStop(AddonInfo addonInfo) {
		// TODO Auto-generated method stub
		// 移除插件菜单
		MenuManager.me().deleteMenuGroup(MENU_KEY);

		// 停止数据库插件
		AddonRecordPluginUtil.stop(addonInfo);
	}

}
