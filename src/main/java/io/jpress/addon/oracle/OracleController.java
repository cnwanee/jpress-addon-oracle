package io.jpress.addon.oracle;

import com.jfinal.core.ActionKey;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;

import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.core.menu.annotation.AdminMenu;
import io.jpress.web.base.AdminControllerBase;

@RequestMapping(value = "/admin/oracle", viewPath = "/")
public class OracleController extends AdminControllerBase{

	@AdminMenu(groupId = OracleAddon.MENU_KEY, text = "数据测试",order = 1)
	public void index() {
		try {
			String s=Db.use(OracleAddon.DB_KEY).queryStr("select 'aaa' from dual");
			setAttr("data", s);
		}catch(Exception e) {
			setAttr("data", e.getMessage());
		}
		render("admin/oracle/index.html");
	}
	
	@AdminMenu(groupId = OracleAddon.MENU_KEY, text = "数据库配置",order = 0)
	public void dbsetting() {
		render("admin/oracle/dbsetting.html");
	}
	
	public void testDB() {
		String config=getPara("db.config");
		try {
			AddonRecordPluginUtil.testLink(config);
			renderJson(Ret.ok().set("msg", "数据库连接成功！"));
		}catch(Exception e) {
			renderJson(Ret.fail().set("msg","数据库连接失败！"+"<br>"+e.getMessage()));
		}
	}
}
