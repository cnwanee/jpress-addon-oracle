# jpress-addon-oracle

#### 介绍
JPRESS在插件中连接oracle的演示代码

#### 软件架构
这个工具demo帮助在jpress插件添加oracle支持


#### 安装教程

1. 手动添加oracle驱动

mvn install:install-file -Dfile=D:\app\product\11.2.0\dbhome_1\jdbc\lib\ojdbc6.jar -DgroupId=com.oracle -DartifactId=jdbc6 -Dversion=6 -Dpackaging=jar
D:\app\product\11.2.0\dbhome_1\jdbc\lib\ojdbc6.jar：此为ojdbc6.jar绝对路径

2. module-oracle oracle默认字段名大写，这是数据库字段名转小写模块，方便和jpress一致
	需在jpress项目 starter pom.xml 添加依赖
	<dependency>
		<groupId>io.jpress</groupId>
		<artifactId>module-oracle</artifactId>
		<version>2.0</version>
	</dependency>
	编译 jpress

3. 编译jpress-addon-oracle插件

4. 启动jpress，添加jpress-addon-oracle插件
成功之后可以看到
Oracle数据库
->数据库配置
->数据测试
可以编辑并测试数据库配置，修改后重启插件生效

请主要看一下三个代码

5. AddonRecordPluginUtil.java 是加载工具
6. OracleAddon.java 插件安装启动类
7. OracleController.java 控制器类，目前偷懒，只写了SQL取数据的样例

        
注意：jpress需2.0.6以上。


