package io.jpress.module.oracle;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;

public class OracleContainerFactory extends CaseInsensitiveContainerFactory {
	OracleContainerFactory(){
		super(true);
	}
}
